﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Name : NetworkBehaviour {

    private const string NAME_MANAGER_GAMEOBJECT = "Name Manager";

    [SerializeField]
    Text _nameText;

    private NameManager _nameMngr;

    [SyncVar(hook = "OnNameChanged")]
    private string _playerName;

    [SyncVar(hook = "OnPlayerIdChanged")]
    private int _playerId = -1;

    public void OnPlayerIdChanged(int playerId) {
        _playerId = playerId;
    }

    public void Start() {
        if (isServer) {
            InitializeName();
        }
    }

    private void InitializeName() {
        if (_playerId == -1) {
            _playerId = GetComponent<NetworkIdentity>().connectionToClient.connectionId;
        }
        if (!_nameMngr) {
            _nameMngr = GameObject.Find(NAME_MANAGER_GAMEOBJECT).GetComponent<NameManager>();
        }
        _playerName = _nameMngr.GetName(_playerId);
        if (_playerName == NameManager.NAME_ERROR) {
            _playerName = _nameMngr.AddName(_playerId, NameManager.DEFAULT_NAME + (_playerId + 1));
        }
    }

    public override void OnStartClient() {
        base.OnStartClient();
        _nameText.text = _playerName;
    }

    public void OnNameChanged(string newname) {
        _playerName = newname;
        _nameText.text = newname;
    }

}
