﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NameManager : NetworkBehaviour {

    public const string DEFAULT_NAME = "Jugador";
    public const string NAME_ERROR = "**ERROR**";
    private const int MAX_TRIES = 50;

    [SyncVar]
    private SyncListInt _playerIds = new SyncListInt();

    [SyncVar]
    private SyncListString _names = new SyncListString();

    public string AddName(int playerId, string playerName) {
        int i = 0;
        string r = playerName;
        while (i < _playerIds.Count && _playerIds[i] != playerId) {
            i++;
        }
        // Si no se encuentra el id del jugador, se intentará añadir el nombre.
        if (i == _playerIds.Count) {
            int j;
            int nTry = 0;
            bool nameAvailable = false;
            // Añadir números al final del nombre si este no está disponible.
            while (nTry < MAX_TRIES && !nameAvailable) {
                j = 0;
                while (j < _playerIds.Count && _names[j] != r) {
                    j++;
                }
                if (j == _names.Count) {
                    nameAvailable = true;
                }
                else {
                    nTry++;
                    r = playerName + nTry;
                }
            }
            if (nTry == MAX_TRIES) {
                r = NAME_ERROR;
            }
            _playerIds.Add(playerId);
            _names.Add(r);
        }
        return r;
    }

    public string GetName(int playerId) {
        string r = NAME_ERROR;
        int i = 0;
        while (i < _playerIds.Count && _playerIds[i] != playerId) {
            i++;
        }
        // Si se encuentra el id, se devuelve el nombre asociado.
        if (i != _playerIds.Count) {
            r = _names[i];
        }
        return r;
    }
   
}
